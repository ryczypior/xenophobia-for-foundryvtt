import ActorXenophobia from "./modules/actor/ActorXenophobia";
import ActorSheetXenophobia from "./modules/actor/sheet/ActorSheetXenophobia";
import ActorSheetXenophobiaCharacter from "./modules/actor/sheet/ActorSheetXenophobiaCharacter";
import ItemSheetXenophobia from "./modules/item/sheet/ItemSheetXenophobia";
import ItemXenophobia from "./modules/item/ItemXenophobia";
import XenophobiaSystem from "./modules/system/XenophobiaSystem"
import registerHooks from "./modules/system/hooks.js"
import ItemSheetXenophobiaEquipment from "./modules/item/sheet/ItemSheetXenophobiaEquipment";
import ItemSheetXenophobiaPowers from "./modules/item/sheet/ItemSheetXenophobiaPowers";
import ItemSheetXenophobiaPerks from "./modules/item/sheet/ItemSheetXenophobiaPerks";
import ItemSheetXenophobiaHouses from "./modules/item/sheet/ItemSheetXenophobiaHouses";
import ActorSheetXenophobiaMonster from "./modules/actor/sheet/ActorSheetXenophobiaMonster";
import ActorSheetXenophobiaNPC from "./modules/actor/sheet/ActorSheetXenophobiaNPC";
import ChatMessageXenophobia from "./modules/system/ChatMessageXenophobia";
import ItemSheetXenophobiaWeapons from "./modules/item/sheet/ItemSheetXenophobiaWeapons";
Hooks.once("init", async function () {
  // Register sheet application classes
  Actors.unregisterSheet('core', ActorSheet);
  Actors.registerSheet('xenophobia', ActorSheetXenophobiaCharacter, {types: ['Character'], makeDefault: true});
  Actors.registerSheet('xenophobia', ActorSheetXenophobiaNPC, {types: ['NPC'], makeDefault: true});
  Actors.registerSheet('xenophobia', ActorSheetXenophobiaMonster, {types: ['Monster'], makeDefault: true});
  Items.unregisterSheet('core', ItemSheet);
  Items.registerSheet('xenophobia', ItemSheetXenophobiaEquipment, {types: ['Equipment'], makeDefault: true});
  Items.registerSheet('xenophobia', ItemSheetXenophobiaPowers, {types: ['Powers'], makeDefault: true});
  Items.registerSheet('xenophobia', ItemSheetXenophobiaPerks, {types: ['Perks'], makeDefault: true});
  Items.registerSheet('xenophobia', ItemSheetXenophobiaHouses, {types: ['Houses'], makeDefault: true});
  Items.registerSheet('xenophobia', ItemSheetXenophobiaWeapons, {types: ['Weapons'], makeDefault: true});

  game.xenophobia = {
    apps: {
      ActorSheetXenophobia,
      ItemSheetXenophobia,
    },
    entities: {
      ActorXenophobia,
      ItemXenophobia,
    },
    config: XenophobiaSystem,
    chat: ChatMessageXenophobia
  };
  CONFIG.Actor.documentClass = ActorXenophobia;
  CONFIG.Item.documentClass = ItemXenophobia;
});

registerHooks();
