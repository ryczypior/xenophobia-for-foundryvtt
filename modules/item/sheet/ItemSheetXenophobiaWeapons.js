import ItemSheetXenophobia from "./ItemSheetXenophobia";

export default class ItemSheetXenophobiaWeapons extends ItemSheetXenophobia {
  get template() {
    return "systems/xenophobia/templates/itemsheet/weapons.html";
  }


  activateListeners(html) {
    super.activateListeners(html);
  }
}
