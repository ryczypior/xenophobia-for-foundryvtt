import ItemSheetXenophobia from "./ItemSheetXenophobia";

export default class ItemSheetXenophobiaPerks extends ItemSheetXenophobia {
  get template() {
    return "systems/xenophobia/templates/itemsheet/perks.html";
  }


  activateListeners(html) {
    super.activateListeners(html);
  }
}
