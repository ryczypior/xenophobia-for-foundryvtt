import ItemSheetXenophobia from "./ItemSheetXenophobia";

export default class ItemSheetXenophobiaEquipment extends ItemSheetXenophobia {
  get template() {
    return "systems/xenophobia/templates/itemsheet/equipment.html";
  }


  activateListeners(html) {
    super.activateListeners(html);
  }
}
