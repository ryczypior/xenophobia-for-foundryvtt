import ItemSheetXenophobia from "./ItemSheetXenophobia";

export default class ItemSheetXenophobiaPowers extends ItemSheetXenophobia {
  get template() {
    return "systems/xenophobia/templates/itemsheet/powers.html";
  }


  activateListeners(html) {
    super.activateListeners(html);
  }
}
