import ItemSheetXenophobia from "./ItemSheetXenophobia";

export default class ItemSheetXenophobiaHouses extends ItemSheetXenophobia {
  get template() {
    return "systems/xenophobia/templates/itemsheet/houses.html";
  }


  activateListeners(html) {
    super.activateListeners(html);
  }
}
