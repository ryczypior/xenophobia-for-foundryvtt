import XenophobiaTests from "./XenophobiaTests";

export default class XenophobiaSkillTest {
  constructor(skillData, actor) {
    if(skillData.reroll){
      this.data = skillData;
    } else {
      const skills = duplicate(actor._source.system.skills);
      const attributes = duplicate(actor._source.system.attributes);
      const skill = skillData.skill;
      let thresholdSkill1 = parseInt(skill.value, 10);
      let thresholdSkill2 = null;
      let thresholdSum = thresholdSkill1;
      if (skillData.skillId !== skillData.skillSectionId){
        thresholdSkill2 = parseInt(skills[skillData.skillSectionId].skills[skillData.skillSectionId].value, 10);
        thresholdSum += thresholdSkill2;
      }
      let thresholdScary = parseInt(actor._source.system.status.fear.value, 10)*-2;
      thresholdSum += thresholdScary;
      this.data = {
        title: game.i18n.localize('TEST.SkillCheck') + ': ' + game.i18n.localize(skill.label),
        thresholdAttr: 0,
        thresholdSkill1: thresholdSkill1,
        thresholdSkill2: thresholdSkill2,
        thresholdBonus: 0,
        thresholdScary: thresholdScary,
        thresholdSum: thresholdSum,
        testName: skill.label,
        callback: skill.callback,
        skillData: skillData,
        skills: skills,
        attributes: attributes,
        skill: skill,
        actor: actor,
        roll1: null,
        roll2: null,
        rollMode: "roll",
        reroll: false,
      };
    }
  }

  async prepareTest() {
    const callback = (html) => {
      this.roll();
    };
    let html = await renderTemplate("/systems/xenophobia/templates/tests/skill-test.html", this.data);

    return new Promise((resolve, reject) => {
      new Dialog(
        {
          title: this.data.title,
          content: html,
          actor: this.data.actor,
          data: this.data,
          buttons:
            {
              rollButton:
                {
                  label: game.i18n.localize("TEST.Roll"),
                  callback: html => resolve(callback(html))
                }
            },
          default: "rollButton",
          render: html => this.activateListeners(html)
        }, {
          width: 600,
          classes: ['xenophobiaroll'],
        }).render(true);
    })
  }

  async _showDiceSoNice(roll, rollMode, speaker) {
    if (game.modules.get("dice-so-nice") && game.modules.get("dice-so-nice").active) {

      if (game.settings.get("dice-so-nice", "hideNpcRolls")) {
        let actorType = null;
        if (speaker.actor)
          actorType = game.actors.get(speaker.actor).type;
        else if (speaker.token && speaker.scene)
          actorType = game.scenes.get(speaker.scene).tokens.get(speaker.token).actor.type;
        if (actorType != "character")
          return;
      }

      let whisper = null;
      let blind = false;
      let sync = true;
      switch (rollMode) {
        case "blindroll": //GM only
          blind = true;
        case "gmroll": //GM + rolling player
          let gmList = game.users.filter(user => user.isGM);
          let gmIDList = [];
          gmList.forEach(gm => gmIDList.push(gm.id));
          whisper = gmIDList;
          break;
        case "selfroll":
          sync = false;
          break;
        case "roll": //everybody
          let userList = game.users.filter(user => user.active);
          let userIDList = [];
          userList.forEach(user => userIDList.push(user.id));
          whisper = userIDList;
          break;
      }
      await game.dice3d.showForRoll(roll, game.user, sync, whisper, blind);
    }
  }

  async _onRerollClick(event){
    this.data.reroll = true;
    this.data.actor.useWillPowerPoint();
    const test = new XenophobiaSkillTest(this.data, this.data.actor);

  }

  async roll() {
    if(this.data.reroll !== false){
      let roll = await new Roll('2d20').roll({async: true});
      await this._showDiceSoNice(roll, this.data.rollMode || "roll", this.data.speaker);
      if(this.data.reroll === 1){
        this.data.roll1 = roll;
      } else {
        this.data.roll2 = roll;
      }
    } else {
      let roll1 = await new Roll('2d20').roll({async: true});
      let roll2 = await new Roll('2d20').roll({async: true});
      await this._showDiceSoNice(roll1, this.data.rollMode || "roll", this.data.speaker);
      await this._showDiceSoNice(roll2, this.data.rollMode || "roll", this.data.speaker);
      this.data.roll1 = roll1;
      this.data.roll2 = roll2;
    }
    if(this.callback){
      await this.callback();
    }
    await this.showTest();
  }

  async showTest(){
    console.log(this.data);
    const wpPoints = parseInt(this.data.actor.system.status.wp.value, 10);
    const canReRoll = game.settings.get('xenophobia', 'allowToRerollAllTests') || (this.data.reroll === false && wpPoints > 0);
    let successes = 0;
    let roll1success = false;
    let roll2success = false;
    if(this.data.roll1.total <= this.data.thresholdSum){
      successes += 1;
      roll1success = true;
    }
    if(this.data.roll2.total <= this.data.thresholdSum){
      successes += 1;
      roll2success = true;
    }
    if(this.data.roll1.terms[0].results[0].result === 1 && this.data.roll1.terms[0].results[1].result === 1){
      successes += 1;
      roll1success = true;
    }
    if(this.data.roll2.terms[0].results[0].result === 1 && this.data.roll2.terms[0].results[1].result === 1){
      successes += 1;
      roll2success = true;
    }
    const templateData = {
      title: this.data.title,
      threshold: this.data.thresholdSum,
      roll1: parseInt(this.data.roll1.total, 10),
      roll2: parseInt(this.data.roll2.total, 10),
      roll11: parseInt(this.data.roll1.terms[0].results[0].result, 10),
      roll12: parseInt(this.data.roll1.terms[0].results[1].result, 10),
      roll21: parseInt(this.data.roll2.terms[0].results[0].result, 10),
      roll22: parseInt(this.data.roll2.terms[0].results[1].result, 10),
      roll1success: roll1success,
      roll2success: roll2success,
      successes: successes,
      actorName: this.data.actor ? this.data.actor.name : null,
      actorImage: this.data.actor ? this.data.actor.img : null,
      canReRoll: canReRoll
    };
    let html = await renderTemplate("/systems/xenophobia/templates/chat/roll/skill.html", templateData);
    const chatOptions = {
      content: html,
      type: 0,
      user: game.user.id,
      speaker: ChatMessage.getSpeaker(this.data.speaker),
      rollMode: this.data.rollMode,
      flags: { roll: this.data, actor: this.data.actor },
    };
    if (["gmroll", "blindroll"].includes(chatOptions.rollMode)) {
      chatOptions["whisper"] = ChatMessage.getWhisperRecipients("GM").map(u => u.id);
    }
    if (chatOptions.rollMode === "blindroll") {
      chatOptions["blind"] = true;
    } else if (chatOptions.rollMode === "selfroll") {
      chatOptions["whisper"] = [game.user];
    }
    await ChatMessage.create(chatOptions);
  }

  _changeThresholdSum(element){
    this.data.thresholdSum = this.data.thresholdSkill1 + parseInt(this.data.thresholdSkill2, 10) + parseInt(this.data.thresholdBonus, 10) + parseInt(this.data.thresholdAttr, 10) + parseInt(this.data.thresholdScary, 10);
    element.querySelector('.thresholdSum').innerHTML = this.data.thresholdSum;
  }
  async _onAttributeClick(event){
    if(event && event.preventDefault){
      event.preventDefault();
    }
    this.data.thresholdAttr = this.data.attributes[event.target.getAttribute('data-attr')].value;
    this._changeThresholdSum(event.target.closest('.xenophobiaroll'));
    event.target.closest('.xenophobiaroll').querySelectorAll('.select-attribute').forEach(node => {
      node.classList.remove('selected');
    });
    event.target.classList.add('selected');
  }

  async _onBonusChange(event){
    this.data.thresholdBonus = parseInt(event.target.value, 10);
    this._changeThresholdSum(event.target.closest('.xenophobiaroll'));
  }

  async _onRollModeChange(event){
    this.data.rollMode = event.target.value;
  }
  activateListeners(html) {
    html.find('.select-attribute').click(this._onAttributeClick.bind(this));
    html.find('.bonusModifier').change(this._onBonusChange.bind(this));
    html.find('.roll-mode').change(this._onRollModeChange.bind(this));
  }
}
