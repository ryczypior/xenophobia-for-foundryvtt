export default class XenophobiaTests {
  constructor(data, actor) {
    if (!data) {
      data = {};
    }
    /**
     *
     * @type [{}]
     */
    this.callback = data.callback;
    this.actor = actor;
    this.data = {
      title: data.title,
      test: data.test,
      roll: data.roll,
      testName: data.testName,
      difficulty: data.difficulty,
      bonus: data.bonus,
      bonusModifier: data.bonusModifier,
      rollMode: data.rollMode,
      reroll: data.reroll || false,
      messageId: data.messageId,
      speaker: data.speaker,
      targets: data.targets,
      opposedMessageIds: data.opposedMessageIds || [],
    }
    if (this.data.speaker && this.actor.isOpposing && this.data.targets.length) {
      ui.notifications.notify(game.i18n.localize("TEST.TargetingCancelled"))
      this.data.targets = [];
    }

    if (!this.data.speaker && actor) {
      this.data.speaker = actor.speakerData();
    }
  }

  /**
   * Add support for the Dice So Nice module
   * @param {Object} roll
   * @param {String} rollMode
   */
  async _showDiceSoNice(roll, rollMode, speaker) {
    if (game.modules.get("dice-so-nice") && game.modules.get("dice-so-nice").active) {

      if (game.settings.get("dice-so-nice", "hideNpcRolls")) {
        let actorType = null;
        if (speaker.actor)
          actorType = game.actors.get(speaker.actor).type;
        else if (speaker.token && speaker.scene)
          actorType = game.scenes.get(speaker.scene).tokens.get(speaker.token).actor.type;
        if (actorType != "character")
          return;
      }

      let whisper = null;
      let blind = false;
      let sync = true;
      switch (rollMode) {
        case "blindroll": //GM only
          blind = true;
        case "gmroll": //GM + rolling player
          let gmList = game.users.filter(user => user.isGM);
          let gmIDList = [];
          gmList.forEach(gm => gmIDList.push(gm.id));
          whisper = gmIDList;
          break;
        case "selfroll":
          sync = false;
          break;
        case "roll": //everybody
          let userList = game.users.filter(user => user.active);
          let userIDList = [];
          userList.forEach(user => userIDList.push(user.id));
          whisper = userIDList;
          break;
      }
      await game.dice3d.showForRoll(roll, game.user, sync, whisper, blind);
    }
  }

  async roll() {
    let roll = await new Roll(this.data.test).roll({async: true});
    await this._showDiceSoNice(roll, this.data.rollMode || "roll", this.data.speaker);
    this.data.roll = roll.total;
    if(this.callback){
      await this.callback();
    }
    await this.showTest();
  }

  async _onDifficultyClick(event){
    if(event && event.preventDefault){
      event.preventDefault();
    }
    this.data.difficulty = parseInt(event.target.getAttribute('data-difficulty'), 10);
    event.target.parentNode.querySelectorAll('.difficulty-button').forEach(node => {
      node.classList.remove('selected');
    });
    event.target.classList.add('selected');
  }

  async _onBonusChange(event){
    this.data.bonusModifier = parseInt(event.target.value, 10);
  }

  async _onRollModeChange(event){
    this.data.rollMode = event.target.value;
  }

  activateListeners(html){
    html.find('.difficulty-button').click(this._onDifficultyClick.bind(this));
    html.find('.bonus').change(this._onBonusChange.bind(this));
    html.find('.roll-mode').change(this._onRollModeChange.bind(this));
  }

  async showTest(){
    const canReRoll = game.settings.get('xenophobia', 'allowToRerollAllTests') || parseInt(this.data.roll, 10) === 1;
    const templateData = {
      title: this.data.title,
      difficulty: this.data.difficulty,
      bonus: parseInt(this.data.bonus, 10),
      bonusModifier: parseInt(this.data.bonusModifier, 10),
      roll: parseInt(this.data.roll, 10),
      result: parseInt(this.data.roll) + parseInt(this.data.bonusModifier, 10) + parseInt(this.data.bonus,10),
      actorName: this.actor ? this.actor.name : null,
      actorImage: this.actor ? this.actor.img : null,
      isPassed: true,
      isCritical: false,
      canReRoll: canReRoll,
    };
    templateData.isPassed = templateData.result >= templateData.difficulty;
    if(templateData.roll === 1){
      templateData.isPassed = false;
      templateData.isCritical = true;
    }
    if(templateData.roll === 10){
      templateData.isPassed = true;
      templateData.isCritical = true;
    }
    let html = await renderTemplate("/systems/xenophobia/templates/chat/roll/skill.html", templateData);
    const chatOptions = {
      content: html,
      type: 0,
      sound: CONFIG.sounds.dice,
      user: game.user.id,
      speaker: ChatMessage.getSpeaker(this.data.speaker),
      rollMode: this.data.rollMode,
      flags: { roll: this.data, actor: this.actor },
    };
    if (["gmroll", "blindroll"].includes(chatOptions.rollMode)) {
      chatOptions["whisper"] = ChatMessage.getWhisperRecipients("GM").map(u => u.id);
    }
    if (chatOptions.rollMode === "blindroll") {
      chatOptions["blind"] = true;
    } else if (chatOptions.rollMode === "selfroll") {
      chatOptions["whisper"] = [game.user];
    }
    await ChatMessage.create(chatOptions);
  }

};
