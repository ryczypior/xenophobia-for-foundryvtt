export default function () {
  /**
   * Init function loads tables, registers settings, and loads templates
   */
  Hooks.once("init", () => {
    console.log('init xenophobia');
    loadTemplates([
      "systems/xenophobia/templates/charactersheet/character.html",
      "systems/xenophobia/templates/charactersheet/partials/attributes.html",
      "systems/xenophobia/templates/charactersheet/partials/equipment.html",
      "systems/xenophobia/templates/charactersheet/partials/houses.html",
      "systems/xenophobia/templates/charactersheet/partials/perks.html",
      "systems/xenophobia/templates/charactersheet/partials/powers.html",
      "systems/xenophobia/templates/charactersheet/partials/skills.html",
      "systems/xenophobia/templates/charactersheet/partials/status.html",
      "systems/xenophobia/templates/charactersheet/partials/weapons.html",
      "systems/xenophobia/templates/itemsheet/equipment.html",
      "systems/xenophobia/templates/itemsheet/houses.html",
      "systems/xenophobia/templates/itemsheet/perks.html",
      "systems/xenophobia/templates/itemsheet/powers.html",
      "systems/xenophobia/templates/itemsheet/weapons.html",
      "systems/xenophobia/templates/itemsheet/partials/item-all-header.html",
      "systems/xenophobia/templates/itemsheet/partials/item-all-description.html",
      "systems/xenophobia/templates/tests/skill-test.html",
      "systems/xenophobia/templates/chat/message.html",
      "systems/xenophobia/templates/chat/roll/skill.html",
    ]);
    game.settings.register("xenophobia", "defaultDifficultyNumber", {
      name: "SETTINGS.defaultDifficultyNumberName",
      hint: "SETTINGS.defaultDifficultyNumberHint",
      scope: "world",
      config: true,
      default: 9,
      type: Number
    });
    game.settings.register("xenophobia", "allowToRerollAllTests", {
      name: "SETTINGS.allowToRerollAllTestsName",
      hint: "SETTINGS.allowToRerollAllTestsHint",
      scope: "world",
      config: true,
      default: false,
      type: Boolean
    });
  });
}
