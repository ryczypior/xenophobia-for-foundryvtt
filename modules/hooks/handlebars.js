export default function () {

    Hooks.on("init", () => {
        Handlebars.registerHelper("ifIsGM", function (options) {
            return game.user.isGM ? options.fn(this) : options.inverse(this)
        })

        Handlebars.registerHelper("isGM", function (options) {
            return game.user.isGM
        })

        Handlebars.registerHelper("eq", function (val1, val2) {
            return val1 == val2;
        })

        Handlebars.registerHelper("in", function (val1, val2) {
            const arry = String(val2).split('|');
            return arry.indexOf(val1) !== -1;
        })

        Handlebars.registerHelper("config", function (key) {
            return game.xenophobia.config[key]
        })

        Handlebars.registerHelper("configLookup", function (obj, key) {
            if (obj && key)
                return game.xenophobia.config[obj]?.[key]
            
        })

        Handlebars.registerHelper("array", function (array, cls) {
            if (typeof cls == "string")
                return array.map(i => `<a class="${cls}">${i}</a>`).join(`<h1 class="${cls} comma">, </h1>`)
            else
                return array.join(", ")
        })

        Handlebars.registerHelper("tokenImg", function(actor) {
            let tokens = actor.getActiveTokens();
            let tokenDocument = actor.prototypeToken;
            if(tokens.length == 1) {
                tokenDocument = tokens[0].document;
            }
            return tokenDocument.hidden ? "systems/xenophobia/tokens/unknown.png" : tokenDocument.texture.src;
        })

        Handlebars.registerHelper("tokenName", function(actor) {
            let tokens = actor.getActiveTokens();
            let tokenDocument = actor.prototypeToken;
            if(tokens.length == 1) {
                tokenDocument = tokens[0].document;
            }
            return tokenDocument.hidden ? "???" : tokenDocument.name;
        })

        Handlebars.registerHelper("settings", function (key) {
            return game.settings.get("xenophobia", key);
        })
})
}
