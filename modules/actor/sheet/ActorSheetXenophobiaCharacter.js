import ActorSheetXenophobia from "./ActorSheetXenophobia";

export default class ActorSheetXenophobiaCharacter extends ActorSheetXenophobia {

  validItemTypes = [
    'Perks',
    'Houses',
    'Powers',
    'Equipment',
    'Weapons',
  ];

  get template() {
    let template = super.template;
    return "systems/xenophobia/templates/charactersheet/character.html";
  }

  activateListeners(html) {
    super.activateListeners(html);
  }
}
