import XenophobiaSystem from "../system/XenophobiaSystem";

export default class ActorXenophobia extends Actor {

  speakerData(token) {
    if (this.isToken || token) {
      return {
        token: token?.id || this.token.id,
        scene: token?.parent.id || this.token.parent.id
      }
    }
    return {
      actor: this.id,
      token: token?.id,
      scene: token?.parent.id
    }
  }

  async changeSkillValue(skill, value){
    const skills = duplicate(this._source.system.skills);
    value = Number(value);
    for(let skillSectionId in skills){
      for(let skillId in skills[skillSectionId].skills){
        if(skillId === skill){
          skills[skillSectionId].skills[skillId].value = value;
        }
      }
    }
    return await this.update({"system.skills": skills});
  }

  async changeAttrValue(attr, value){
    const attributes = duplicate(this._source.system.attributes);
    value = Number(value);
    for(let attrId in attributes){
      if(attrId === attr){
        attributes[attrId].value = value;
      }
    }
    return await this.update({"system.attributes": attributes});
  }

  async useWillPowerPoint(){
    const status = duplicate(this._source.system.status);
    if(status.wp.value > 0){
      status.wp.value-=1;
    }
    return await this.update({"system.status": status});
  }

  findItemByInternalID(internalId, type = null) {

  }

  async _preCreate(data, options, user) {
    await super._preCreate(data, options, user);
    await this.updateSource({
      "system.skills": duplicate(XenophobiaSystem.Skills),
      "system.attributes": duplicate(XenophobiaSystem.Attributes)
    });
  }

  prepareBaseData() {
    super.prepareBaseData();
  }
}

