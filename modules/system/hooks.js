import * as initHooks from "../hooks/init.js"
import * as handlebarsHooks from "../hooks/handlebars.js";
import ChatMessageXenophobia from "./ChatMessageXenophobia";

export default function registerHooks() {
  console.log('registerHooks');
  initHooks.default();
  handlebarsHooks.default();


  // #if _ENV === "development"
  Hooks.on("renderApplication", (app, html, data) => {
  });
  //#endif
  Hooks.on('renderChatMessage', (message, html, data) => {
    ChatMessageXenophobia.activateListeners(html, message, data);
  });

}
