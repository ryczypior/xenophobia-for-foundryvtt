import XenophobiaUtils from "./XenophobiaUtils";
import XenophobiaSkillTest from "../tests/XenophobiaSkillTest";

export default class ChatMessageXenophobia {
  static async activateListeners (html, message, data){
    html.find('.reroll').click(ev => ChatMessageXenophobia._onReRollClick(ev, message, data));
  }

  static async _onReRollClick(event, message, data){
    const actor = XenophobiaUtils.findActorById(message.flags.roll.actor._id);
    if(!actor){
      ui.notifications.warn(game.i18n.localize('CHAT.ActorNotFound'));
      return;
    }
    const status = duplicate(actor._source.system.status);
    if(status.wp.value <= 0){
      ui.notifications.warn(game.i18n.localize('CHAT.NoWPPoints'));
      return;
    }
    status.wp.value = status.wp.value - 1;
    await actor.update({"system.status": status});
    const templateData = {
      title: game.i18n.localize('CHAT.ReRollUsingWPTitle'),
      message: game.i18n.format('CHAT.ReRollUsingWPMessage', {actorName: actor.name, testName: game.i18n.localize(message.flags.roll.testName)}),
      actorName: actor.name,
      actorImage: actor.img,
    };
    let html = await renderTemplate("/systems/xenophobia/templates/chat/message.html", templateData);
    const chatOptions = {
      content: html,
      type: 0,
      user: game.user.id,
      speaker: ChatMessage.getSpeaker(actor.speakerData()),
    };
    await ChatMessage.create(chatOptions);
    const rollData = message.flags.roll;
    rollData.reroll = parseInt(event.target.getAttribute('data-reroll'), 10);
    const test = new XenophobiaSkillTest(rollData, message.flags.roll.actor);
    test.roll();
  }


}
