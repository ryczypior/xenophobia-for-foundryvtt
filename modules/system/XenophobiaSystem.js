const XenophobiaSystem = {};

XenophobiaSystem.Attributes = {
  "physical": {
    "label": "SHEET.Attributes.Physical",
    "value": 5,
  },
  "social": {
    "label": "SHEET.Attributes.Social",
    "value": 5,
  },
  "mental": {
    "label": "SHEET.Attributes.Mental",
    "value": 5,
  }
}
XenophobiaSystem.Skills = {
  "combat": {
    "skills": {
      "combat" : {
        "label": "SHEET.Skills.Combat",
        "value": 1,
        "min": 1,
        "max": 99,
        "main": 1,
      },
      "fighting": {
        "label": "SHEET.Skills.Fighting",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "melee": {
        "label": "SHEET.Skills.MeleeWeapons",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "firearm": {
        "label": "SHEET.Skills.Firearm",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "throwing": {
        "label": "SHEET.Skills.Throwing",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      }
    }
  },
  "movement": {
    "skills": {
      "movement": {
        "label": "SHEET.Skills.Movement",
        "value": 1,
        "min": 1,
        "max": 99,
        "main": 1,
      },
      "athletics": {
        "label": "SHEET.Skills.Athletics",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "acrobatics": {
        "label": "SHEET.Skills.Acrobatics",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "hiding": {
        "label": "SHEET.Skills.Hiding",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "driving": {
        "label": "SHEET.Skills.Driving",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      }
    }
  },
  "knowledge": {
    "skills": {
      "knowledge": {
        "label": "SHEET.Skills.Knowledge",
        "value": 1,
        "min": 1,
        "max": 99,
        "main": 1,
      },
      "education": {
        "label": "SHEET.Skills.Education",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "science": {
        "label": "SHEET.Skills.Science",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "technology": {
        "label": "SHEET.Skills.Technology",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "occult": {
        "label": "SHEET.Skills.Occult",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "medicine": {
        "label": "SHEET.Skills.Medicine",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      }
    }
  },
  "speech": {
    "skills": {
      "speech": {
        "label": "SHEET.Skills.Speech",
        "value": 1,
        "min": 1,
        "max": 99,
        "main": 1,
      },
      "persuasion": {
        "label": "SHEET.Skills.Persuasion",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "intimidation": {
        "label": "SHEET.Skills.Intimidation",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "lying": {
        "label": "SHEET.Skills.Lying",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "seduction": {
        "label": "SHEET.Skills.Seduction",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      }
    }
  },
  "survival": {
    "skills": {
      "survival": {
        "label": "SHEET.Skills.Survival",
        "value": 1,
        "min": 1,
        "max": 99,
        "main": 1,
      },
      "tracking": {
        "label": "SHEET.Skills.Tracking",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "perception": {
        "label": "SHEET.Skills.Perception",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "persistence": {
        "label": "SHEET.Skills.Persistence",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
      "focus": {
        "label": "SHEET.Skills.Focus",
        "value": 0,
        "min": 0,
        "max": 99,
        "main": 0,
      },
    }
  }
}

export default XenophobiaSystem;
